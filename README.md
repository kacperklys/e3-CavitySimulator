
e3-cavitysimulator  
======
ESS Site-specific EPICS IOC Application : cavitysimulator

# Description

To limit risk connected with tests and measurement on real resonant cavities (Medium Beta and High Beta types), the hardware cavity simulator was delivered. It emulates the behaviour of cavities and klystron using signals from LLRF system.

This IOC was designed to configure and control cavity simulator. You can find panel operator in `opi` directory. 

More detailed information can be found [here](https://confluence.esss.lu.se/display/RFG/Cavity+Simulator+IOC+installation+and+usage)

# Requirements

The IP address of the cavity simulator must be known to configure properly cmd file. 
Before intallation, E3 environment must be set up [link](https://github.com/icshwi/e3)

## How to install and run IOC

 1. Clone repository
 ```sh
 git clone https://gitlab.esss.lu.se/kacperklys/e3-CavitySimulator
 ```

 2. Go to e3 directory and set environment variables
 ```sh
 cd e3
 source tools/setenv
 ```
 
 3. Go to cmds file directory and open CavitySimulator.cmd file
 ```sh
 cd ~/e3-cavitysimulator/cmds
 vim CavitySimulator.cmd
 ```

 4. Check if versions of stream and iocStats modules are compatible with those, installed on your local machine. If not, you can change version numbers or install proper modules' versions.

 5. It is important to insert the proper IP of the Cavity Simulator (here, it is  10.1.3.79 port 7)
 ```sh
epicsEnvSet("IP_ADDR", "10.1.3.79:7")
 ```

 6. Build and Install IOC
 ```sh
 cd ~/e3-cavitysimulator
 make build
 make install
 ```

 7. Run IOC
 ```sh
 iocsh.bash cmds/CavitySimulator.cmd
 ```

## Deployment
In `deploy` directory there is `deploy.sh` script which creates service with IOC along with necessary directories and files.
The script creates new directory names `cavitysimulator` in `/iocs/` with .sh files, log directory, run directory and sock directory with socket which can be used to connect with active IOC.
In `/etc/systemd/system` it generates service file with IOC.

To see how the script can be used type:
```sh
./deploy.sh -h
``` 

Using script, a user can define several parameters:
 - cavity simulator module version -> flag `-z`
 - iocStats module version -> flag `-i`
 - autosave module version -> flag `-u`
 - recsync module version -> flag `-c`
 - stream module version -> flag `-t`
 - name of the service file -> flag `-n`
 - decide if ioc should be started on the fly -> flag `-s`
 - ip address of the cavity simulator -> flag `-a`
 - macro $(P) value -> flag `-p`
 - macro $(R) value -> flag `-r`

The only obligatory flag that needs to passed is `-a`, defining ip address of the cavity simulator. The others have default values.
In case of modules, by default, the script takes the newest versions, IOC is not started after deployment. The default service file name is `ioc@CavityTest.service`.
P macro value is LLRF_USER, R macro value is CavitySimulator.

Example usage:
```sh
./deploy.sh -a 10.1.3.79:7 -n ioc@CS.service
``` 

## GUI operator panels

Interface is composed of 11 windows:
 * Main menu
 * Status
 * Model configuration
 * Network configuration 
 * Temperature
 * Calibration and Reference Signal configuration
 * Piezo and Lorentz
 * Expert Configuration
 * Slow Resonance Drift
 * Beam Current

Before launching OPIs, you need to change macros definitions in Main menu window. Then, macros values are distributed to all panels. That is why you need to launch operator interface starting with Main menu window (presented below).

![Main window](opi/bob/images/main_window.PNG?raw=true)

### Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### Author Information
In case of problems contact with :email: kklys@mail.dmcs.pl
