require cavitysimulator

##########################
### Cavity Simulator iocsh
##########################
epicsEnvSet("IP_ADDR", "10.1.3.79:7")
epicsEnvSet("PORT", "myConnection")
epicsEnvSet("P", "LLRF_USER:")
epicsEnvSet("R", "CavitySimulator:")

iocshLoad("$(cavitysimulator_DIR)/cs.iocsh", "PORT=$(PORT), IP_ADDR=$(IP_ADDR), P=$(P), R=$(R)")  

iocInit()

