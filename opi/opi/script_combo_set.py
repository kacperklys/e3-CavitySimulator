from org.csstudio.opibuilder.scriptUtil import ConsoleUtil
from org.csstudio.opibuilder.scriptUtil import PVUtil
import WidgetUtil

lvl2 = PVUtil.getString(pvs[0])
lvl3 = PVUtil.getString(pvs[1])
lvl4 = pvs[2]

label_name = "Combo_2"

ATT_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7", "ADC"]
GATE_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7"]
CONST_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7"]
MUX_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7"]
SYNC_array = ["MODE", "DELAY", "ON", "OFF"]
AWG_array = ["SIN", "TRAF", "TRAR", "TRAC", "FALL", "RIS", "SQON", "SQOFF", "TRIG", "OUT"]
AMP_array = ["MODE", "Q", "GAIN"]
MODEX_array = ["Q", "GAIN", "DETU"]
PIEZO_array = ["GAIN"]
DRIFT_array = ["STATUS"]

if lvl2 == "ATT":
	WidgetUtil.setItems(display, label_name, ATT_array)
elif lvl2 == "GATE":
	WidgetUtil.setItems(display, label_name, GATE_array)
elif lvl2 == "CONSTI" or lvl2 == "CONSTQ":
	WidgetUtil.setItems(display, label_name, CONST_array)
elif lvl2 == "MUX":
	WidgetUtil.setItems(display, label_name, MUX_array)
elif lvl2 == "AWG":
	WidgetUtil.setItems(display, label_name, AWG_array)
elif lvl2 == "SYNC":
	WidgetUtil.setItems(display, label_name, SYNC_array)
elif "MOD" in lvl2:
	WidgetUtil.setItems(display, label_name, MODEX_array)
elif lvl2 == "AMP":
	WidgetUtil.setItems(display, label_name, AMP_array)
elif (lvl2 == "PIEZO1" or lvl2 == "PIEZO2" or lvl2 == "PIEZOO"):
	WidgetUtil.setItems(display, label_name, PIEZO_array)
elif lvl2 == "DRIFT":
	WidgetUtil.setItems(display, label_name, DRIFT_array)
	lvl4.setValue("?")
