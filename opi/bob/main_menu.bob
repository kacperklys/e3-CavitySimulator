<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Main menu</name>
  <macros>
    <P>LLRF_USER:</P>
    <R>CavitySimulator:</R>
    <USER>LLRF_USER</USER>
  </macros>
  <width>860</width>
  <height>640</height>
  <widget type="rectangle" version="2.0.0">
    <name>title-bar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>860</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>title</name>
    <class>TITLE</class>
    <text>Cavity Simulator</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>570</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>subtitle</name>
    <class>SUBTITLE</class>
    <text>Main menu</text>
    <x>590</x>
    <y use_class="true">20</y>
    <width>250</width>
    <height use_class="true">30</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">2</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>group-rectangle-1</name>
    <x>200</x>
    <y>70</y>
    <width>430</width>
    <height>490</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
    <tooltip>Threshold panel -&gt; Set new thresholds values and then press commit to load them to module.</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>group-title-1</name>
    <class>HEADER2</class>
    <text>Basic Settings</text>
    <x>200</x>
    <y>70</y>
    <width>430</width>
    <height>30</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>2</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_13</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>$(P)$(R)Reset-SP</pv_name>
    <text>Reset Cavity Simulator</text>
    <x>335</x>
    <y>500</y>
    <width>170</width>
    <height>40</height>
    <tooltip>Reset Cavity Simulator</tooltip>
    <show_confirm_dialog>true</show_confirm_dialog>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Close_button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[from org.csstudio.display.builder.runtime.script import ScriptUtil

ScriptUtil.closeDisplay(widget)]]></text>
        </script>
        <description>Execute Script</description>
      </action>
    </actions>
    <text>Close</text>
    <x>730</x>
    <y>580</y>
    <width>110</width>
    <height>40</height>
    <tooltip>Close window</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button_11</name>
    <actions>
      <action type="open_display">
        <file>Calibration_Reference.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Calibration and EEPROM</description>
      </action>
      <action type="open_display">
        <file>Expert.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Expert</description>
      </action>
      <action type="open_display">
        <file>Status.bob</file>
        <target>window</target>
        <description>Status</description>
      </action>
      <action type="open_display">
        <file>Model.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Model</description>
      </action>
      <action type="open_display">
        <file>Network.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Network</description>
      </action>
      <action type="open_display">
        <file>Temperature.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Temperature</description>
      </action>
      <action type="open_display">
        <file>Slow_Resonance_Drift.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Slow Resonance Drift</description>
      </action>
      <action type="open_display">
        <file>Piezo_Lorentz.bob</file>
        <macros>
          <USER>LLRF_USER</USER>
        </macros>
        <target>window</target>
        <description>Piezo and Lorentz</description>
      </action>
      <action type="open_display">
        <file>Beam.bob</file>
        <target>replace</target>
        <description>Beam</description>
      </action>
    </actions>
    <text>Choose Window</text>
    <x>550</x>
    <y>580</y>
    <width>160</width>
    <height>40</height>
    <tooltip>Choose next window</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>group-4</name>
    <x>230</x>
    <y>120</y>
    <width>370</width>
    <height>180</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>group-rectangle-4</name>
      <width>370</width>
      <height>180</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>group-title-4</name>
      <class>HEADER3</class>
      <text>General Information</text>
      <width>370</width>
      <height>30</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_1</name>
      <text>Identification:</text>
      <x>20</x>
      <y>55</y>
      <width>120</width>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label</name>
      <text>Software Version:</text>
      <x>20</x>
      <y>95</y>
      <width>120</width>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>2</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_9</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)SoftVerBut-SP</pv_name>
      <text>Refresh</text>
      <x>150</x>
      <y>130</y>
      <tooltip>Refresh software version and name of device</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_1</name>
      <pv_name>$(P)$(R)Iden-RB</pv_name>
      <x>150</x>
      <y>50</y>
      <width>200</width>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Name of device</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update</name>
      <pv_name>$(P)$(R)SoftVer-RB</pv_name>
      <x>150</x>
      <y>90</y>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>Software version</tooltip>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Configuration_group</name>
    <x>230</x>
    <y>320</y>
    <width>370</width>
    <height>160</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Configuration_rectangle</name>
      <width>370</width>
      <height>160</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Configuration_label</name>
      <class>HEADER3</class>
      <text>Configuration</text>
      <width>370</width>
      <height>40</height>
      <font use_class="true">
        <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
        </font>
      </font>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Save_button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)saveEEPROM-SP</pv_name>
      <text>Save Current Configuration</text>
      <x>90</x>
      <y>50</y>
      <width>200</width>
      <tooltip>Save current configuration to EEPROM</tooltip>
      <show_confirm_dialog>true</show_confirm_dialog>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Get_button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(P)$(R)getEEPROM-SP</pv_name>
      <text>Load Configuration</text>
      <x>90</x>
      <y>110</y>
      <width>200</width>
      <tooltip>Load configuration from EEPROM</tooltip>
      <show_confirm_dialog>true</show_confirm_dialog>
    </widget>
  </widget>
</display>
