from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil


lvl2 = PVUtil.getString(pvs[0])

ATT_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7", "ADC"]
GATE_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7"]
CONST_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7"]
MUX_array = ["VM1", "VM2", "VM3", "VM4", "VM5", "VM6", "VM7"]
SYNC_array = ["MODE", "DELAY", "ON", "OFF"]
AWG_array = ["SIN", "TRAF", "TRAR", "TRAC", "FALL", "RIS", "SQON", "SQOFF", "TRIG", "OUT"]
AMP_array = ["MODE", "Q", "GAIN"]
MODEX_array = ["Q", "GAIN", "DETUN"]
PIEZO_array = ["GAIN"]
DRIFT_array = ["STATUS"]
REFERENCE_array = ["MODE"]
BEAM_array = ["PHASE", "CURRENT", "RISE", "FALL", "LENGTH"]

if lvl2 == "ATT":
	widget.setItems(ATT_array)
	PVUtil.writePV(str(pvs[2]), ATT_array[0], 500)
elif lvl2 == "GATE":
	widget.setItems(GATE_array)
	PVUtil.writePV(str(pvs[2]), GATE_array[0], 500)
elif lvl2 == "CONSTI" or lvl2 == "CONSTQ":
	widget.setItems(CONST_array)
	PVUtil.writePV(str(pvs[2]), CONST_array[0], 500)
elif lvl2 == "MUX":
	widget.setItems(MUX_array)
	PVUtil.writePV(str(pvs[2]), MUX_array[0], 500)
elif lvl2 == "AWG":
	widget.setItems(AWG_array)
	PVUtil.writePV(str(pvs[2]), AWG_array[0], 500)
elif lvl2 == "SYNC":
	widget.setItems(SYNC_array)
	PVUtil.writePV(str(pvs[2]), SYNC_array[0], 500)
elif "MOD" in lvl2:
	widget.setItems(MODEX_array)
	PVUtil.writePV(str(pvs[2]), MODEX_array[0], 500)
elif lvl2 == "AMP":
	widget.setItems(AMP_array)
	PVUtil.writePV(str(pvs[2]), AMP_array[0], 500)
elif "PIEZ" in lvl2:
	widget.setItems(PIEZO_array)
	PVUtil.writePV(str(pvs[2]), PIEZO_array[0], 500)
elif lvl2 == "DRIFT":
	widget.setItems(DRIFT_array)
	PVUtil.writePV(str(pvs[2]), DRIFT_array[0], 500)
	PVUtil.writePV(str(pvs[1]), "?", 500)
elif lvl2 == "REFERENCE":
	widget.setItems(REFERENCE_array)
	PVUtil.writePV(str(pvs[2]), REFERENCE_array[0], 500)
elif lvl2 == "BEAM":
	widget.setItems(BEAM_array)
	PVUtil.writePV(str(pvs[2]), BEAM_array[0], 500)
