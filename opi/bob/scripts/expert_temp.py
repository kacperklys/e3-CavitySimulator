from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil


meas = PVUtil.getString(pvs[0])
temp = PVUtil.getString(pvs[1])
value = PVUtil.getString(pvs[2])

if meas=="" or temp =="" or value == "":
	ScriptUtil.getLogger().severe("At least one field is empty!")
	flag = 0
else:
	flag = 1

if flag ==1:
	StringToSend = meas+":"+temp+":"+value
	PVUtil.writePV(str(pvs[5]), StringToSend, 500)