from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import csv

path_to_load = PVUtil.getString(pvs[0])
array_with_freq = [pvs[3], pvs[5], pvs[7], pvs[9], pvs[11], pvs[13], pvs[15], pvs[17], pvs[19], pvs[21], pvs[23], pvs[25], pvs[27], pvs[29], pvs[31], pvs[33], pvs[35], pvs[37], pvs[39], pvs[41], pvs[43], pvs[45], pvs[47], pvs[49], pvs[51], pvs[53], pvs[55], pvs[57], pvs[59], pvs[61], pvs[63], pvs[65]]
array_with_time = [pvs[4], pvs[6], pvs[8], pvs[10], pvs[12], pvs[14], pvs[16], pvs[18], pvs[20], pvs[22], pvs[24], pvs[26], pvs[28], pvs[30], pvs[32], pvs[34], pvs[36], pvs[38], pvs[40], pvs[42], pvs[44], pvs[46], pvs[48], pvs[50], pvs[52], pvs[54], pvs[56], pvs[58], pvs[60], pvs[62], pvs[64]]

if len(path_to_load)>2:
	try:
		# gets data from txt file
		data = [row for row in csv.reader(open(path_to_load, 'r'))]
		# sets flag
		is_digit = True
		# checks if in the file are only numbers
		for i in range(1, len(data)):
		# lstrip to check also negative values
			for k in range(len(data[i])):
				if (data[i][k].lstrip("-").isdigit() is False and data[i][k] != "") and (data[1][2] != '-'):
					is_digit = False
					break
			# checks if value is a number (is digit alert is not false)
			if is_digit == False:
				raise ValueError
			# clear spinbox and insert number of rows from file
			PVUtil.writePV(str(pvs[2]), str(len(data)-1), 500)
			# insert corresponding values to entries
			PVUtil.writePV(str(array_with_freq[i-1]), data[i][1], 500)
			if (i!=len(data)-1):
				PVUtil.writePV(str(array_with_time[i-1]), data[i+1][2], 500)
	except IOError:
		ScriptUtil.getLogger().severe("I/O error")
	except ValueError:
		ScriptUtil.getLogger().severe("Forbidden character in file")
	except:
		ScriptUtil.getLogger().severe("Unknown error")
else:
	ScriptUtil.getLogger().severe("Wrong path")
