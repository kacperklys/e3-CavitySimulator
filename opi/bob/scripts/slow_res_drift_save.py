from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import csv


array_with_freq = [pvs[3], pvs[5], pvs[7], pvs[9], pvs[11], pvs[13], pvs[15], pvs[17], pvs[19], pvs[21], pvs[23], pvs[25], pvs[27], pvs[29], pvs[31], pvs[33], pvs[35], pvs[37], pvs[39], pvs[41], pvs[43], pvs[45], pvs[47], pvs[49], pvs[51], pvs[53], pvs[55], pvs[57], pvs[59], pvs[61], pvs[63], pvs[65]]
array_with_time = [pvs[4], pvs[6], pvs[8], pvs[10], pvs[12], pvs[14], pvs[16], pvs[18], pvs[20], pvs[22], pvs[24], pvs[26], pvs[28], pvs[30], pvs[32], pvs[34], pvs[36], pvs[38], pvs[40], pvs[42], pvs[44], pvs[46], pvs[48], pvs[50], pvs[52], pvs[54], pvs[56], pvs[58], pvs[60], pvs[62], pvs[64]]
path_to_save = PVUtil.getString(pvs[0])

if len(path_to_save)>2:
 	# variables
	dict_data = []
        sth_empty = 0
        # arrays needs to filled because when there is error it skips those arrays in the code
        spin_val = PVUtil.getLong(pvs[2])
        # join to arrays
        array = array_with_freq[0:spin_val] + array_with_time[0:spin_val]
        # checks if entry is empty, if yes flag is 1 and loop is being broken
	for k in array:
		if k == '':
                	sth_empty = 1
                	break
        # if all entries have values
        if sth_empty == 0:
            	# full path
		download_dir = path_to_save + "\SRD.txt"
            	# name of the columns
            	csv_columns = ['Number', 'Freq', 'Time']
            	# data to be saved
            	for i in range(0, spin_val):
                	if i!=0:
                    		dict_data.append({'Number': i+1, 'Freq': PVUtil.getLong(array_with_freq[i]), 'Time': PVUtil.getLong(array_with_time[i-1])})
                	else:
                    		dict_data.append({'Number': i + 1, 'Freq': PVUtil.getLong(array_with_freq[i]), 'Time': '-'})
            	# saving values to file
            	try:
                	with open(download_dir, 'wb') as csvfile:
                    		writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
                    		writer.writeheader()
                    		for data in dict_data:
                        		writer.writerow(data)
			ScriptUtil.getLogger().severe("Data has been saved")
            # exceptions
            	except IOError:
			ScriptUtil.getLogger().severe("I/O error")
            	except:
			ScriptUtil.getLogger().severe("Unknown error")
	else:
		ScriptUtil.getLogger().severe("Something is empty")

else:
	ScriptUtil.getLogger().severe("Path is empty")
