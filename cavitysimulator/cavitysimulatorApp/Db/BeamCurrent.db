# Beam current configuration

##########
# Setpoints
record(longout, "$(P)$(R)BeamPhase-SP") {
    field(DESC, "Sets phase of the beam")
    field(DTYP, "stream")
    field(OUT,  "@BeamCurrent.proto setPhase() $(PORT)")
    field(LOPR, "0")
    field(HOPR, "360")
    field(EGU,  "deg")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BeamBut-SP")
}

record(longout, "$(P)$(R)BeamCurrent-SP") {
    field(DESC, "Sets beam current")
    field(DTYP, "stream")
    field(OUT,  "@BeamCurrent.proto setCurr() $(PORT)")
    field(SCAN, "Passive")
    field(LOPR, "0")
    field(FLNK, "$(P)$(R)BeamBut-SP")
}

record(longout, "$(P)$(R)BeamFall-SP") {
    field(DESC, "Sets fall time of beam profile")
    field(DTYP, "stream")
    field(OUT,  "@BeamCurrent.proto setFall() $(PORT)")
    field(LOPR, "0")
    field(EGU,  "ns")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BeamBut-SP")
}

record(longout, "$(P)$(R)BeamRise-SP") {
    field(DESC, "Sets rise time of beam profile")
    field(DTYP, "stream")
    field(OUT,  "@BeamCurrent.proto setRise() $(PORT)")
    field(LOPR, "0")
    field(EGU,  "ns")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BeamBut-SP")
}

record(longout, "$(P)$(R)BeamLength-SP") {
    field(DESC, "Sets beam length")
    field(DTYP, "stream")
    field(OUT,  "@BeamCurrent.proto setLength() $(PORT)")
    field(LOPR, "0")
    field(EGU,  "ns")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BeamBut-SP")
}

record(ao, "$(P)$(R)BeamBut-SP") {
    field(DESC, "Get beam params")
    field(FLNK, "$(P)$(R)BeamPhase-RB")
    field(SCAN, "Passive")
    field(PINI, "YES")
}
##########
# Readouts
record(longin, "$(P)$(R)BeamPhase-RB") {
    field(DESC, "Reads phase of the beam")
    field(DTYP, "stream")
    field(INP,  "@BeamCurrent.proto getPhase() $(PORT)")
    field(SCAN, "Passive")
    field(EGU,  "deg")
    field(FLNK, "$(P)$(R)BeamCurrent-RB")
}

record(ai, "$(P)$(R)BeamCurrent-RB") {
    field(DESC, "Reads current of the beam")
    field(DTYP, "stream")
    field(INP,  "@BeamCurrent.proto getCurr() $(PORT)")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BeamFall-RB")
}

record(longin, "$(P)$(R)BeamFall-RB") {
    field(DESC, "Reads fall time")
    field(DTYP, "stream")
    field(INP,  "@BeamCurrent.proto getFall() $(PORT)")
    field(SCAN, "Passive")
    field(EGU,  "ns")
    field(FLNK, "$(P)$(R)BeamRise-RB")
}

record(longin, "$(P)$(R)BeamRise-RB") {
    field(DESC, "Reads rise time of beam profile")
    field(DTYP, "stream")
    field(INP,  "@BeamCurrent.proto getRise() $(PORT)")
    field(EGU,  "ns")
    field(SCAN, "Passive")
    field(FLNK, "$(P)$(R)BeamLength-RB")
}

record(longin, "$(P)$(R)BeamLength-RB") {
    field(DESC, "Reads beam length")
    field(DTYP, "stream")
    field(INP,  "@BeamCurrent.proto getLength() $(PORT)")
    field(SCAN, "Passive")
    field(EGU,  "ns")
}
