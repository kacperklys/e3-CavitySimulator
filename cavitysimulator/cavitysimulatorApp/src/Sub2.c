#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <stdlib.h>

static long Sub2(aSubRecord *prec)
{
	epicsInt32 *number_of_points = (epicsInt32*)prec->a;
	epicsInt32 *time11 = (epicsInt32*)prec->b;
	epicsInt32 *freq11 = (epicsInt32*)prec->c;
	epicsInt32 *time12 = (epicsInt32*)prec->d;
	epicsInt32 *freq12 = (epicsInt32*)prec->e;
	epicsInt32 *time13 = (epicsInt32*)prec->f;
	epicsInt32 *freq13 = (epicsInt32*)prec->g;
	epicsInt32 *time14 = (epicsInt32*)prec->h;
	epicsInt32 *freq14 = (epicsInt32*)prec->i;
	epicsInt32 *time15 = (epicsInt32*)prec->j;
	epicsInt32 *freq15 = (epicsInt32*)prec->k;
	epicsInt32 *time16 = (epicsInt32*)prec->l;
	epicsInt32 *freq16 = (epicsInt32*)prec->m;
	epicsInt32 *time17 = (epicsInt32*)prec->n;
	epicsInt32 *freq17 = (epicsInt32*)prec->o;
	epicsInt32 *time18 = (epicsInt32*)prec->p;
	epicsInt32 *freq18 = (epicsInt32*)prec->r;
	epicsInt32 *time19 = (epicsInt32*)prec->s;
	epicsInt32 *freq19 = (epicsInt32*)prec->t;
	epicsInt32 *prev_array = (epicsInt32*)prec->u;
	int *wave = (int*)prec->vale;
	int *size = (int*)prec->vald;
	int *send = (int*)prec->valf;
	int *next_sub = (int*)prec->valg;
	int *enable_wave3 = (int*)prec->valb;
	int *enable_wave2 = (int*)prec->valc;
	int *enable_wave4 = (int*)prec->vala;

	int time_array[9] = {*time11, *time12, *time13, *time14, *time15, *time16, *time17, *time18, *time19};
	int freq_array[9] = {*freq11, *freq12, *freq13, *freq14, *freq15, *freq16, *freq17, *freq18, *freq19};


	if(*number_of_points>10)
	{
		if(*number_of_points>19)
		{
			// enable waveform 3
			*enable_wave3 = 1;
			// disable waveform 2
			*enable_wave2 = 0;
		}
		else
		{
			// enable waveform 2
			*enable_wave2 = 1;
			//disable waveforms 3 and 4
			*enable_wave4 = 0;
			*enable_wave3 = 0;
		}
		
		int i;
		int counter=0;
		int counter2=0;
		int num_copy;
		if(*number_of_points>19)
		{
			num_copy = 19;
		}
		else
		{
			num_copy = *number_of_points;
		}
		*size = num_copy*2-1;
		
		// 19 is max number of points from previous waveform
		// copy values from previous waveform
		for(i=0; i<19; i++)
		{
			wave[i] = prev_array[i];
		}
		// insert new points to waveform
		for(i=19; i<*size; i++)
		{
			if(i%2)
			{
				if(time_array[counter]==0)
				{
					return 1;
				}
				if(time_array[counter]<0 || time_array[counter]>10000000)
				{
					return 2;
				}
				wave[i] = time_array[counter];
				counter++;
			}
			else
			{
				if(freq_array[counter2]<-10000000 || freq_array[counter2]>10000000)
				{
					return 2;
				}
				wave[i] = freq_array[counter2];
				counter2++;
			}
		}
		if(*number_of_points>19)
		{
			// activate next subroutine
			*next_sub = 1;
		}
		else
		{
			// activate record with sending procedure
			*send = 1;
		}
	}
	else
	{
		// disbale waveform 2
		*enable_wave2 = 0;
	}
		
	return 0;
	
}

epicsRegisterFunction(Sub2);
