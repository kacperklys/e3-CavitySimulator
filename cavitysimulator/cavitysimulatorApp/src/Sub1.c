#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <stdlib.h>

static long Sub1(aSubRecord *prec)
{
	epicsInt32 *number_of_points = (epicsInt32*)prec->a;
	epicsInt32 *freq1 = (epicsInt32*)prec->b;
	epicsInt32 *time2 = (epicsInt32*)prec->c;
	epicsInt32 *freq2 = (epicsInt32*)prec->d;
	epicsInt32 *time3 = (epicsInt32*)prec->e;
	epicsInt32 *freq3 = (epicsInt32*)prec->f;
	epicsInt32 *time4 = (epicsInt32*)prec->g;
	epicsInt32 *freq4 = (epicsInt32*)prec->h;
	epicsInt32 *time5 = (epicsInt32*)prec->i;
	epicsInt32 *freq5 = (epicsInt32*)prec->j;
	epicsInt32 *time6 = (epicsInt32*)prec->k;
	epicsInt32 *freq6 = (epicsInt32*)prec->l;
	epicsInt32 *time7 = (epicsInt32*)prec->m;
	epicsInt32 *freq7 = (epicsInt32*)prec->n;
	epicsInt32 *time8 = (epicsInt32*)prec->o;
	epicsInt32 *freq8 = (epicsInt32*)prec->p;
	epicsInt32 *time9 = (epicsInt32*)prec->r;
	epicsInt32 *freq9 = (epicsInt32*)prec->s;
	epicsInt32 *time10 = (epicsInt32*)prec->t;
	epicsInt32 *freq10 = (epicsInt32*)prec->u;
	int *wave = (int*)prec->vald;
	int *size = (int*)prec->valc;
	int *send = (int*)prec->vale;
	int *next_sub = (int*)prec->valf;
	int *enable_wave2 = (int*)prec->vala;
	int *enable_wave1 = (int*)prec->valb;

	int time_array[9] = {*time2, *time3, *time4, *time5, *time6, *time7, *time8, *time9, *time10};
	int freq_array[10] = {*freq1, *freq2, *freq3, *freq4, *freq5, *freq6, *freq7, *freq8, *freq9, *freq10};
	
	int i;
	int num_copy;
	if(*number_of_points>10)
	{
		num_copy = 10;
	}
	else
	{
		num_copy = *number_of_points;
	}
	*size = num_copy*2-1;
	int counter = 1;
	int counter2 = 1;
	// Insert values to waveform
	for(i=0; i<*size; i++)
	{
		if(i==0)
		{
			if(freq_array[i]==0)
			{
				return 1;
			}
			if(freq_array[i]<-10000000 || freq_array[i]>10000000)
			{
				return 2;
			}
			wave[i] = freq_array[i];
		}
		else if(i%2)
		{
			if(i==1)
			{
				if(time_array[i-counter]==0)
				{
					return 1;
				}
				if(time_array[i-counter]<0 || time_array[i-counter]>10000000)
				{
					return 2;
				}
				wave[i] = time_array[i-counter];
			}
			else
			{
				if(time_array[i-counter]==0)
				{
					return 1;
				}
				if(time_array[i-counter]<0 || time_array[i-counter]>10000000)
				{
					return 2;
				}
				wave[i] = time_array[i-counter];
			}
			counter++;
		}
		else
		{
			if(i==2)
			{
				if(freq_array[i-counter2]<-10000000 || freq_array[i-counter2]>10000000)
				{
					return 2;
				}
				wave[i] = freq_array[i-counter2];
			}
			else
			{
				if(freq_array[i-counter2]<-10000000 || freq_array[i-counter2]>10000000)
				{
					return 2;
				}
				wave[i] = freq_array[i-counter2];
			}
			counter2++;
		}
	}
	if(*number_of_points>10)
	{
		// activate  next subroutine
		*next_sub = 1;
		// enable waveform 2
		*enable_wave2 = 1;
		// disable waveform 1
		*enable_wave1 = 0;
	}
	else
	{
		// enable waveform 1
		*enable_wave1 = 1;
		// disable waveform 2
		*enable_wave2 = 0;
		// activate record with sending procedure
		*send = 1;
	}
	return 0;
	
}

epicsRegisterFunction(Sub1);
