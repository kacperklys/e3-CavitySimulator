#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <stdlib.h>

static long Sub4(aSubRecord *prec)
{
	epicsInt32 *number_of_points = (epicsInt32*)prec->a;
	epicsInt32 *time29 = (epicsInt32*)prec->b;
	epicsInt32 *freq29 = (epicsInt32*)prec->c;
	epicsInt32 *time30 = (epicsInt32*)prec->d;
	epicsInt32 *freq30 = (epicsInt32*)prec->e;
	epicsInt32 *time31 = (epicsInt32*)prec->f;
	epicsInt32 *freq31 = (epicsInt32*)prec->g;
	epicsInt32 *time32 = (epicsInt32*)prec->h;
	epicsInt32 *freq32 = (epicsInt32*)prec->i;
	epicsInt32 *prev_array = (epicsInt32*)prec->j;
	int *wave = (int*)prec->valb;
	int *size = (int*)prec->vala;
	int *send = (int*)prec->valc;

	int time_array[4] = {*time29, *time30, *time31, *time32};
	int freq_array[4] = {*freq29, *freq30, *freq31, *freq32};
	
	if(*number_of_points>28)
	{
		int i;
		int counter=0;
		int counter2=0;
		int num_copy;
		
		num_copy = *number_of_points;
		
		*size = num_copy*2-1;
		
		// 55 is max number of points from previous subroutines		
		// Copy  values from previous waveform to new one
		for(i=0; i<55; i++)
		{
			wave[i] = prev_array[i];
		}

		// insert new values to waveform
		for(i=55; i<*size; i++)
		{
			if(i%2)
			{
				if(time_array[counter] == 0)
				{
					return 1;
				}
				if(time_array[counter]<0 || time_array[counter]>10000000)
				{
					return 2;
				}
				wave[i] = time_array[counter];
				counter++;
			}
			else
			{
				if(freq_array[counter2]<-10000000 || freq_array[counter2]>10000000)
				{
					return 2;
				}

				wave[i] = freq_array[counter2];
				counter2++;
			}
		}
		// Enable record with sending procedure
		*send = 1;
		
	}
	
	return 0;
	
}

epicsRegisterFunction(Sub4);
