#include <dbDefs.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>

#include <stdio.h>
#include <stdlib.h>

static long Sub3(aSubRecord *prec)
{
	epicsInt32 *number_of_points = (epicsInt32*)prec->a;
	epicsInt32 *time20 = (epicsInt32*)prec->b;
	epicsInt32 *freq20 = (epicsInt32*)prec->c;
	epicsInt32 *time21 = (epicsInt32*)prec->d;
	epicsInt32 *freq21 = (epicsInt32*)prec->e;
	epicsInt32 *time22 = (epicsInt32*)prec->f;
	epicsInt32 *freq22 = (epicsInt32*)prec->g;
	epicsInt32 *time23 = (epicsInt32*)prec->h;
	epicsInt32 *freq23 = (epicsInt32*)prec->i;
	epicsInt32 *time24 = (epicsInt32*)prec->j;
	epicsInt32 *freq24 = (epicsInt32*)prec->k;
	epicsInt32 *time25 = (epicsInt32*)prec->l;
	epicsInt32 *freq25 = (epicsInt32*)prec->m;
	epicsInt32 *time26 = (epicsInt32*)prec->n;
	epicsInt32 *freq26 = (epicsInt32*)prec->o;
	epicsInt32 *time27 = (epicsInt32*)prec->p;
	epicsInt32 *freq27 = (epicsInt32*)prec->r;
	epicsInt32 *time28 = (epicsInt32*)prec->s;
	epicsInt32 *freq28 = (epicsInt32*)prec->t;
	epicsInt32 *prev_array = (epicsInt32*)prec->u;
	int *wave = (int*)prec->vald;
	int *size = (int*)prec->valc;
	int *send = (int*)prec->vale;
	int *next_sub = (int*)prec->valf;
	int *enable_wave4 = (int*)prec->vala;
	int *enable_wave3 = (int*)prec->valb;


	int time_array[9] = {*time20, *time21, *time22, *time23, *time24, *time25, *time26, *time27, *time28};
	int freq_array[9] = {*freq20, *freq21, *freq22, *freq23, *freq24, *freq25, *freq26, *freq27, *freq28};

	if(*number_of_points>19)
	{
		if(*number_of_points>28)
		{
			*enable_wave4 = 1;
			*enable_wave3 = 0;
		}
		else
		{
			*enable_wave4 = 0;
			*enable_wave3 = 1;
		}
		
		int i;
		int counter=0;
		int counter2=0;
		int num_copy;
		if(*number_of_points>28)
		{
			num_copy = 28;
		}
		else
		{
			num_copy = *number_of_points;
		}
		*size = num_copy*2-1;

		// 37 is max number of points from previous waveforms
		// Copy values from previous waveform
		for(i=0; i<37; i++)
		{
			wave[i] = prev_array[i];
		}
		// Insert new points to waveform
		for(i=37; i<*size; i++)
		{
			if(i%2)
			{
				if(time_array[counter]==0)
				{
					return 1;
				}
				if(time_array[counter]<0 || time_array[counter]>10000000)
				{
					return 2;
				}
				wave[i] = time_array[counter];
				counter++;
			}
			else
			{
				if(freq_array[counter2]<-10000000 || freq_array[counter2]>10000000)
				{
					return 2;
				}
				wave[i] = freq_array[counter2];
				counter2++;
			}
		}
		if(*number_of_points>28)
		{
			// activate next subroutine
			*next_sub = 1;
		}
		else
		{
			// activate record with sending procedure
			*send = 1;
		}
	}
	else
	{
		// disable waveform 3
		*enable_wave3 = 0;
	}

	if(*number_of_points <19)
	{
		// disable waveform 4
		*enable_wave4 = 0;
	}
	
	return 0;	
}

epicsRegisterFunction(Sub3);
