#!/bin/bash

# global variables
logPath="\${SITEAPP}/log/log-ioc-%i" 
serviceName="ioc@CavityTest.service"
serviceTemplate="ioc@Template.service"
logTemplate="ioc@Template.log-conf"
startFlag=0
siteApp=/iocs/cavitysimulator
pMacro="LLRF_USER"
rMacro="CavitySimulator"
cavityVer=""

# Usage info
show_help() {
cat << EOF
This script is used to generate service file from template and to add it to systemd.
You can use 'n' flag to define service file name. By default it is 'ioc@CavityTest.service'
Running script without any flags means loading default values to generated file.
When '-s' flag is passed, script will start ioc just after deployment
With flags '-p' and 'r' macros for IOC can be defined.
procServ is creating socket for IOC. This socket is used to connect to IOC console.
It can be found in /iocs/<IOC>/sock/. To connect to socket: nc -U <socket_name>.
Usage: ${0##*/} [-n name] [-h] [-s] [-p macro] [-r macro] [-a ip_addr] [-z cavitysimulator_ver]
	-h	display this help and exit
	-n	define service file name
	-s	when flag passed, script will start ioc just after deployment
	-r	define \$(R) macro
	-p	define \$(P) macro
	-z	cavitysimulator module version
	-t	stream module version
	-a	ip address of the Cavity Simulator
EOF
}

# check if the given name is allowed as filename
check_name() {
if ! [[ $1 =~ ^[0-9a-zA-Z._-@]+$ ]]; then
	echo 'Only 0-9a-zA-Z._-@ characters are allowed'
        exit 1
fi
serviceName=$1
}

# check if template exists
check_template() {
if [ ! -f "$1" ]; then
	    echo "$1 does not exist"
	    exit 1
fi
}

check_siteApp() {
if [ ! -d $siteApp ]; then
	sudo mkdir -p "$siteApp"
fi
if [ ! -d $siteApp/log ]; then
	sudo mkdir -p "$siteApp/log/"
fi
if [ ! -d $siteApp/run ]; then
	sudo mkdir -p "$siteApp/run/"
fi
if [ ! -d $siteApp/sock ]; then
	sudo mkdir -p "$siteApp/sock/"
fi
sudo chown -R $(whoami) $siteApp
sudo chown -R $(whoami):$(whoami) "$siteApp/sock/"
}

copy_cmd_sh() {
	cp ../cmds/CavitySimulator.cmd.service "$siteApp/"
	count=$(find ../cavitysimulator-loc/iocsh -type f -name "*.iocsh" | wc -l)
	if [ "$count" -ne 0 ]; then
		cp -R ../cavitysimulator-loc/iocsh/. "$siteApp/"	
	fi
}

define_p() {
	if [ ! -z "$1" ]; then
		if ! [[ $1 =~ ^[0-9a-zA-Z_-]+$ ]]; then
			echo '$(P) -> Only 0-9a-zA-Z_- characters are allowed'
        		exit 1
		fi
		pMacro=$1
	fi
}

define_r() {
	if [ ! -z "$1" ]; then
		if ! [[ $1 =~ ^[0-9a-zA-Z_-]+$ ]]; then
			echo '$(R) -> Only 0-9a-zA-Z_- characters are allowed'
        		exit 1
		fi
		rMacro=$1
	fi
}

check_ip() {
	if [ -z "$1" ]; then
		echo 'Pass IP address of the Cavity Simulator'
		exit 1
	else
		if [[ $1 =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+:[0-9]+ ]]; then
			ipAddr=$1
		else
			echo 'Wrong IP address format'
			exit 1
		fi
	fi
}

check_modules() {
	versionsInstalled=$(make -C $EPICS_SRC/e3-$1 existent 2>/dev/null | grep -e '── [0-9]' | cut -d' ' -f2 | tail -1 )
	echo ${versionsInstalled}
}

#####################################################

while getopts ":n:p:r:z:a:t:hs" opt; do
	case ${opt} in
		n )
			check_name $OPTARG
			;;
		h )
			show_help
			exit 0
			;;
		t )	streamVer=$OPTARG
			;;

		a )	check_ip $OPTARG
			;;

		z )
			cavityVer=$OPTARG
			;;

		p )
			define_p $OPTARG
			;;
		r )
			define_r $OPTARG
			;;
		s )
			startFlag=1
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
	esac
done

#####################################################

# check if directory siteApp exists
check_siteApp 

# check if templates exists
check_template $serviceTemplate
check_template $logTemplate

# check available modules by default take the newest
if [ -z "$cavityVer" ]; then
	cavityVer=$(check_modules cavitysimulator)
fi

if [ -z "$streamVer" ]; then
	streamVer=$(check_modules StreamDevice)
fi

if [ -z "$ipAddr" ]; then
	echo "Missing IP address of the cavity simulator"
	exit 1
fi

# copy template
cp ./ioc@Template.service ./"$serviceName"
logTemplateName="${serviceName%.service}.log-conf"
cp ./ioc@Template.log-conf ./"$logTemplateName"

# copy cmd and iocsh
copy_cmd_sh

# get epics base directory from env variable
epicsPath=$(echo $EPICS_BASE)
if [ -z "$epicsPath" ]; then
	echo "EPICS_BASE variable is not defined"
	exit 1
fi

# get E3 require 
e3RequirePath=$(echo $E3_REQUIRE_BIN)
if [ -z "$e3RequirePath" ]; then
	echo "E3_REQUIRE_BIN variable is not defined"
	exit 1
fi
iocshPath="${e3RequirePath}/iocsh.bash"

# get cmd directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
top=${DIR%deploy}
cmdsPath="${top}cmds"
# extract socketname
socketName="${serviceName%.service}"

# sed can't deal with '/' in path
logPathSed=$( echo "$logPath" | sed -r 's/\//__/g')
siteAppSed=$( echo "$siteApp" | sed -r 's/\//__/g')
cmdsPathSed=$( echo "$cmdsPath" | sed -r 's/\//__/g')
iocshPathSed=$( echo "$iocshPath" | sed -r 's/\//__/g') 

# Configure service file
sed -i "s/LOGFILE/$logPathSed/" "$serviceName"
sed -i "s/CMDSPATH/$siteAppSed/g" "$serviceName"
sed -i "s/IOCSHPATH/$iocshPathSed/" "$serviceName"
sed -i "s/PMACRO/$pMacro/" "$serviceName"
sed -i "s/RMACRO/$rMacro/" "$serviceName"
sed -i "s/SOCKETNAME/$socketName/" "$serviceName"
sed -i "s/CAVITYVER/$cavityVer/" "$serviceName"
sed -i "s/STREAMVER/$streamVer/" "$serviceName"
sed -i "s/IPADDR/$ipAddr/" "$serviceName"

## bring back '/'
sed -i "s/__/\//g" "$serviceName"

# Configure log configuration file
sed -i "s/IOCNAME/${siteApp#/iocs/}/" "$logTemplateName" 

# move to /etc/systemd/system
sudo mv ./"$serviceName" /etc/systemd/system

# systemctl stuff
sudo systemctl enable "$serviceName" > /dev/null 2>&1
if [ ! -L "/etc/systemd/system/multi-user.target.wants/$serviceName" ]; then
	echo "$serviceName is not enabled."
fi

sudo systemctl daemon-reload > /dev/null 2>&1 
if [ $startFlag -eq 1 ]; then
	sudo systemctl start "$serviceName"
	state=$(systemctl is-active "$serviceName")
	if [ "$state" != "active" ]; then
		echo "$serviceName not started"
		exit 1
	else
		echo "$serviceName started"
	fi
fi

#Make sure the logs get rotated
[[ ! -n $(rpm -qa | grep logrotate) ]] && echo "Missing package for logrotate. No log management implemented for IOC service."
sed "s/{{ whoami }}/$(whoami)/g" < "$pathTemplates""$logTemplateName" | sudo tee /etc/logrotate.d/"${serviceName%.service}" >/dev/null

# cleaning
rm  -rf "$logTemplateName" 
